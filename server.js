var mysql = require('mysql');
var express = require('express');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var im = require('imagemagick');
var multipartMiddleware = multipart();
var fs = require('fs');
var digest = require('./digest.js');
var app = express();

var PORT = process.env.PORT || 8080;
var LIST_PER_PAGE = 10;
var MESSAGE_SUCCESS = 'success';
var MESSAGE_ERROR = 'error';

app.use(bodyParser());

// allow cross domain ajax request
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// production server
var connection = mysql.createConnection({
  //host     : '192.168.0.53',
  host     : 'localhost',
  user     : 'root',
  password : 'z742096001',
  database : 'leadcapture'
});

// development server
/*
var connection = mysql.createConnection({
  host     : '192.168.56.101',
  user     : 'admin',
  password : 'Lembang1',
  database : 'honda'
});
*/

function searchIndexOfPhoto(photos, searchId) {
  for(var i in photos) {
    if(photos[i].record_id == searchId)
      return i;
  }

  return -1;
}

connection.connect(function(err) {
  if (err) {
    console.error('error connecting to database'); 
    process.exit(1);
    return;
  }

  console.log('connected as id ' + connection.threadId);
});

app.get('/api', function(req, res) {
  console.log('Test Client connected to Server');
  res.send('Connected');
});

/*
 * @method
 *   POST
 *
 * @resource
 *   login
 *
 * @description
 *   login using give username password
 * 
 * @body
 *   username : login username
 *   password : login password
 */
app.post('/api/login', function(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  var query = 'select * from t_user where username = "' + username + '" and password = "' + password + '"';

  connection.query(query, function(err, rows, fields) {
    if(err) {
      res.send({msg : MESSAGE_ERROR});
      return;
    };
    if(rows.length == 0) {
      res.send({msg : MESSAGE_ERROR});
      return;
    }
    res.send({msg : MESSAGE_SUCCESS, 'user' : rows[0]});
  });
});

/*
 * @method
 *   POST
 *
 * @resource
 *   user
 *
 * @description
 *   create a new user
 * 
 * @body
 *   username : new username
 *   password : new password
 */
app.post('/api/user', function(req, res) {
  var query = 'insert into t_user set ?';
  var record = {}; 

  record.username = req.body.username;
  record.password = digest.SHA256(req.body.password);
  connection.query(query, record, function(err, result) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
    }
    res.send({msg : MESSAGE_SUCCESS});
  });
});

/*
 * @method
 *   GET
 *
 * @resouce
 *   records
 *
 * @description
 *   can be used to extract data from database based on USERNAME or DATE, or both
 *
 * @query
 *   username : selection username
 *   date     : selection date with format 'yyyy-mm-dd'
 *
 * @example
 *   http://server:8080/api/records?name=test&date=2014-05-23
 */
app.get('/api/records', function (req, res) {
  var query = 'select t_record.* from t_record inner join t_user on t_record.owner_id = t_user.id where ';
  var where = '';

  if(req.query.username)
    where = 't_user.username = "' + req.query.username + '"';

  if(req.query.date) {
    if(where)
      where += ' and ';
    where += 'DATE(t_record.timestamp) = "' + req.query.date + '"';
  }

  if(req.query.name) {
    where.name = req.query.trim().replace(/\s+/, ' ').split(' ');
  }

  if(where)
    where += ' and ';
  where += 'deletion_flag = 0';

  var orderBy = ' order by t_record.id';
 
  var result = {};
  connection.query(query + where + orderBy, function(err, records, field) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    var record_ids = new Array();
    for(var i in records) {
      record_ids.push(records[i].id);
    }

    var result = JSON.parse(JSON.stringify(records));
    connection.query('select id, record_id from t_photo where record_id in (?) order by record_id, id', [record_ids], function(err, photos, field) {
      var photoIndex;
      if(err) {
        console.log(err);
        res.send({msg : MESSAGE_ERROR, err : err});
        return;
      }

      for(var i in result) {
        // put photos to result
        photoIndex = searchIndexOfPhoto(photos, result[i].id);
        result[i].photos = [];
        if(photoIndex >= 0) {
          for(var j =  photoIndex; j < photos.length && photos[j].record_id == result[i].id; j++) {
            photos[j].path = 'http://' + req.headers.host + '/api/photo/' + photos[j].id;
            result[i].photos.push(photos[j]);
          }
        }
      }
      res.set('Content/type', 'application/json');
      res.send({result : result});
    });
  });
});

/*
 * @method
 *   GET
 *
 * @resource
 *   records/list
 *
 * @description
 *   display continous list of records
 *
 * @query
 *   id : last ID, ignore this parameter if you want to get list from the first record
 */
app.get('/api/records/list', function(req, res) {
  var where = JSON.parse( JSON.stringify( req.query ));
  var steppingFromId = 0;
  if(where.id) {
    steppingFromId = where.id;
    delete where.id;
  }

  var query = 'select * from t_record where ? and deletion_flag = 0 and id < ' + steppingFromId + ' order by id desc limit ' + LIST_PER_PAGE;

  connection.query(query, where, function(err, rows, fields) {
    if(err) { 
      res.send({msg : 'error', err : err});
      return;
    };

    var records = new Array();
    for(var i in rows) {
      records[i] = {};
      for(var j in rows[i])
        if(rows[i][j] instanceof Date) {
          records[i][j] = (rows[i][j].getMonth() + 1) + "/" + rows[i][j].getDate() + "/" + rows[i][j].getFullYear();
	} else {
          records[i][j] = rows[i][j];
        }
    }

    res.send({msg : MESSAGE_SUCCESS, records : records});

  });

});

/*
 * @method
 *   POST
 *
 * @resource
 *   record
 *
 * @description
 *   post single record
 * 
 * @body
 *   location
 *     latitude
 *     longitude
 *   owner_id
 *   name
 *   sex
 *   street
 *   city
 *   province
 *   cellphone1
 *   cellphone2
 *   fixed_line
 *   email
 *   birth_place
 *   birth_date : yyyy-mm-dd
 *   id_type    : KTP/SIM/PASSPORT
 *   id_number
 *   id_expires_date
 *   leasing
 */
app.post('/api/record', function(req, res) {
  var record = JSON.parse(JSON.stringify(req.body));
  var query = 'insert into t_record set ?, `location_point` = GeomFromText(\'POINT(' + req.body.location.latitude + ' ' + req.body.location.longitude + ')\')';

  // adjust format to meet db's
  record.location = req.body.location.latitude + "," + req.body.location.longitude;

  connection.query(query, record, function(err, result) {
    if(err) {
      console.log(err);
      res.send({msg : 'error', err : err});
      return;
    }

    res.send({msg : MESSAGE_SUCCESS, 'result' : result});
  });

});

/*
 * @method
 *   PUT
 *
 * @resource
 *   record/:id
 *
 * @description
 *   update a single record
 * 
 * @params
 *   id : record id
 *
 * @body
 *   location : latitude,longitude
 *   owner_id
 *   name
 *   sex
 *   street
 *   city
 *   province
 *   cellphone1
 *   cellphone2
 *   fixed_line
 *   email
 *   birth_place
 *   birth_date : yyyy-mm-dd
 *   id_type    : KTP/SIM/PASSPORT
 *   id_number
 *   id_expires_date
 *   leasing
 */
app.put('/api/record/:id', function(req, res) {
  var record = JSON.parse(JSON.stringify(req.body));

  var query = 'update t_record set ?, `location_point` = GeomFromText(\'POINT(' + req.body.location.latitude + ' ' + req.body.location.longitude + ')\')' +
              'where id = ' + req.params.id;

  record.location = req.body.location.latitude + ',' + req.body.location.longitude;
  connection.query(query, record, function(err, result) {
    if(err) {
      console.log(err);
      res.send({msg : 'error', err : err});
      return;
    }
    res.send({msg : MESSAGE_SUCCESS});
  });

});

/*
 * @method
 *   DELETE
 *
 * @resource
 *   record/:id
 *
 * @description
 *   delete record from database by setting deletion_flag to 1
 * 
 * @params
 *   id : record id
 */
app.del('/api/record/:id', function(req, res) {
  var query = 'update t_record set deletion_flag = 1 where id = ' + req.params.id;

  connection.query(query, function(err, result) {
    if(err) {
      console.log(err);
      res.send({msg : 'error', err : err});
      return;
    }
    res.send({msg : MESSAGE_SUCCESS});

    var query = 'update t_photo set deletion_flag = 1 where record_id = ' + req.params.id;
    connection.query(query, function(err, result) {

    });
  });
});

/*
 * @method
 *   GET
 *
 * @resource
 *   photo/:id
 *
 * @description
 *   download single photo
 * 
 * @params
 *   id : photo id
 *
 * @example
 *   http://server:8080/api/photo/12345
 */
app.get('/api/photo/:id', function(req, res) {
  var query = 'select photo from t_photo where deletion_flag = 0 and ?';
  connection.query(query, req.params, function(err, row, field) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    if(!req.query.height && !req.query.width) {
      res.contentType('image/jpeg');
      res.send(row[0].photo);
      return;
    } else {
      im.resize({format : 'jpeg', srcData : row[0].photo, width : req.query.width, height : req.query.height}, function(resizeErr, resizeStdout, resizeStderr) {
        if(resizeErr) {
          console.log(JSON.stringify(resizeErr));
          res.send({msg : MESSAGE_ERROR, err : resizeErr});
          return;
        }
        res.contentType('image/jpeg')
        res.end(resizeStdout, 'binary');
      });
    }
  });
});

/*
 * @method
 *   GET
 *
 * @resource
 *   photos
 *
 * @description
 *   get list of photos based on record id
 * 
 * @query
 *   record_id
 */
app.get('/api/photos', function(req, res) {
  var query = 'select id, taker_id from t_photo where deletion_flag = 0 and ?';

  connection.query(query, req.query, function(err, rows, field) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    res.send({msg : MESSAGE_SUCCESS, photos : rows});
  });
});

/*
 * @method
 *   POST
 *
 * @resource
 *   photo
 *
 * @description
 *   save a photo to database
 * 
 * @body
 *   record_id : this photo belongs to which record
 *   taker_id  : who is the taker of this photo
 *
 * @files
 *   photo to be uploaded
 */
app.post('/api/photo', multipartMiddleware, function(req, res){
  var binImage = fs.readFileSync(req.files.file.path);
  var record = JSON.parse( JSON.stringify(req.body) );
  record.photo = binImage;

  connection.query('insert into t_photo set ?', record, function(err, row) {
    fs.unlink(req.files.file.path, function(err) {
    });

    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    res.send({msg : MESSAGE_SUCCESS});
  });

});

/*
 * @method
 *   DELETE
 *
 * @resource
 *   photo/:id
 *
 * @description
 *   delete a photo by setting deletion flag to 1
 * 
 * @params
 *   id : photo id 
 */
app.del('/api/photo/:id', function(req, res) {
  connection.query('update t_photo set deletion_flag = 1 where ?', {id : req.params.id}, function(err, row) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    res.send({msg : MESSAGE_SUCCESS});
  });
});

/*
 * @method
 *   GET
 *
 * @resource
 *   masters
 *
 * @description
 *   get master data for CITY, PROVINCE, and LEASING
 */
app.get('/api/masters', function(req, res) {
  connection.query('select * from t_master order by "key", value', function(err, rows, fields) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    res.send({msg : MESSAGE_SUCCESS, masters : rows});

  });
});


console.log('listening on port ' + PORT + ' ...' )
app.listen(PORT)
